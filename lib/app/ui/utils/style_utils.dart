import 'package:flutter/material.dart';

const double SIZED_FLOW_STATE = 450;
const double SIZED_STATE_1 = 4;
const double SIZED_STATE_2 = ((SIZED_FLOW_STATE - 50) / 5) * 1;
const double SIZED_STATE_3 = ((SIZED_FLOW_STATE - 50) / 5) * 2;
const double SIZED_STATE_4 = ((SIZED_FLOW_STATE - 50) / 5) * 3;
const double SIZED_STATE_5 = ((SIZED_FLOW_STATE - 50) / 5) * 4;
const double SIZED_STATE_6 = (((SIZED_FLOW_STATE - 50) / 5) * 5) - 4;

const double SIZED_PANEL_ITEM = 40;
const double SIZED_PANEL_ITEM_ORDER = 90;

const Color PRIMARY = Color(0xFF63B4FF);
const Color SECONDARY = Color(0xFFF3F9FF);
const Color WARNING = Color(0xFF3B69C6);
const Color DANGER = Color(0xFF3B69C6);
const Color INFO = Color.fromARGB(255, 66, 78, 101);
const Color NORMAL = Color(0xFF000000);
const Color BACKGROUND = Color(0xFFF1F3F9);
const Color UNNOTICED = Color(0xFF888888);

const double H0 = 40;
const double H1 = 32;
const double H2 = 26;
const double H3 = 22;
const double H4 = 20;
const double H5 = 18;

const double P1 = 13;
const double P2 = 11;

// const TextStyle H1 = TextStyle(fontSize: 20, fontWeight: FontWeight.w500);
// const TextStyle H2 = TextStyle(fontSize: 18, fontWeight: FontWeight.w500);
// const TextStyle H3 = TextStyle(fontSize: 16, fontWeight: FontWeight.w400);
// const TextStyle H4 = TextStyle(fontSize: 13, fontWeight: FontWeight.w300);
// const TextStyle H5 = TextStyle(fontSize: 11, fontWeight: FontWeight.w300);
