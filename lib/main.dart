import 'package:citv_v1_app/app/bindings/presentation_binding.dart';
import 'package:citv_v1_app/app/routes/pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'app/ui/themes/light_theme.dart';

void main() async {
  await GetStorage.init();

  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.INITIAL,
      initialBinding: PresentationBinding(),
      theme: appThemeDataLight,
      defaultTransition: Transition.fade,
      getPages: AppPages.pages,
    ),
  );
}
